package util;


public class Alphabet {

    public static char[] alphabet = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    // public static char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();

    public static int getPosition(char item) {
        for (int i = 0; i < alphabet.length; i++)
            if (alphabet[i] == item) return i;
        return -1;
    }
}
