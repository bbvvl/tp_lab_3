package util;

import java.util.LinkedHashSet;
import java.util.Set;

public class Playfair {

    private String key;
    //[i][j] i столбец j строка
    private char matrix_arr[][] = new char[5][5];

    //encrypt method
    public String encryptMessage(String input) {

        String src_arr[] = getBigrams(input);

        StringBuilder output = new StringBuilder();
        char one;
        char two;
        int part1[];
        int[] part2;
        for (String aSrc_arr : src_arr) {
            one = aSrc_arr.charAt(0);
            two = aSrc_arr.charAt(1);
            part1 = getMatrixPosition(one);
            part2 = getMatrixPosition(two);
            if (part1[0] == part2[0]) {
                if (part1[1] < 4)
                    part1[1]++;
                else
                    part1[1] = 0;
                if (part2[1] < 4)
                    part2[1]++;
                else
                    part2[1] = 0;
            } else if (part1[1] == part2[1]) {
                if (part1[0] < 4)
                    part1[0]++;
                else
                    part1[0] = 0;
                if (part2[0] < 4)
                    part2[0]++;
                else
                    part2[0] = 0;
            } else {
                int temp = part1[1];
                part1[1] = part2[1];
                part2[1] = temp;
            }
            output.append(matrix_arr[part1[0]][part1[1]]).append(matrix_arr[part2[0]][part2[1]]);


        }

        return output.toString();
    }

    private String[] getBigrams(String input) {

        input = format(input);

        int size = input.length();
        if (size % 2 != 0) {
            size++;
            input += 'x';
        }


        String bigrams[] = new String[size / 2];
        int counter = 0;
        for (int i = 0; i < size / 2; i++) {
            bigrams[i] = input.substring(counter, counter + 2);
            counter += 2;
        }

        return bigrams;
    }

    private String format(String input) {

        input = input.replace('j', 'i');
        for (int i = 0; i < input.length(); i = i + 2) {
            if (input.charAt(i + 1) == input.charAt(i)) {
                StringBuilder sb = new StringBuilder(input);
                sb.insert(i + 1, 'x');
                input = sb.toString();
            }
        }
        return input;
    }

    private int[] getMatrixPosition(char letter) {
        int[] key = new int[2];
        if (letter == 'j')
            letter = 'i';
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (matrix_arr[i][j] == letter) {
                    key[0] = i;
                    key[1] = j;
                    break;
                }
            }
        }
        return key;
    }

    //decrypt method
    public String decryptMessage(String input) {

        String src_arr[] = getBigrams(input);

        StringBuilder output = new StringBuilder();
        char one;
        char two;
        int part1[];
        int part2[];
        for (String aSrc_arr : src_arr) {
            one = aSrc_arr.charAt(0);
            two = aSrc_arr.charAt(1);
            part1 = getMatrixPosition(one);
            part2 = getMatrixPosition(two);
            if (part1[0] == part2[0]) {
                if (part1[1] > 0)
                    part1[1]--;
                else
                    part1[1] = 4;
                if (part2[1] > 0)
                    part2[1]--;
                else
                    part2[1] = 4;
            } else if (part1[1] == part2[1]) {
                if (part1[0] > 0)
                    part1[0]--;
                else
                    part1[0] = 4;
                if (part2[0] > 0)
                    part2[0]--;
                else
                    part2[0] = 4;
            } else {
                int temp = part1[1];
                part1[1] = part2[1];
                part2[1] = temp;
            }
            output.append(matrix_arr[part1[0]][part1[1]]).append(matrix_arr[part2[0]][part2[1]]);

        }
        return output.toString();
    }

    // set and get KEY
    public String getTableKey(String key) {
        keyGen(removeDuplicates(key.toCharArray()));
        return matrix();

    }

    private String removeDuplicates(char[] key) {

        Set<Character> charSet = new LinkedHashSet<>();
        for (char c : key) {
            charSet.add(c);
        }

        StringBuilder sb = new StringBuilder();
        for (Character character : charSet) {
            sb.append(character);
        }
        return sb.toString();

    }

    private void keyGen(String preKey) {
        boolean flag = true;
        char current;
        key = preKey;
        for (int i = 0; i < 26; i++) {
            current = (char) (i + 97);
            if (current == 'j')
                continue;
            if (preKey.lastIndexOf(current) != -1) flag = false;

            if (flag)
                key += current;
            flag = true;
        }

    }

    private String matrix() {
        StringBuilder sb = new StringBuilder();
        int counter = 0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                matrix_arr[i][j] = key.charAt(counter);
                sb.append(matrix_arr[i][j]).append(" ");

                counter++;
            }
            sb.append("\n");

        }

        return sb.toString();
    }
}
