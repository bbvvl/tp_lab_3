package controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import util.Playfair;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainController implements Initializable {
    @FXML
    public MenuItem openMenuItem;
    @FXML
    public MenuItem saveMenuItem;
    @FXML
    public MenuItem closeMenuItem;
    @FXML
    private
    TextField textFieldKey;
    @FXML
    Button btnEncrypt;
    @FXML
    Button btnDecrypt;
    @FXML
    private
    TextArea textAreaTableKey;
    @FXML
    private
    TextArea textAreaInput;
    @FXML
    private
    TextArea textAreaOutput;

    private Stage primaryStage;

    public void setPrimaryStage(Stage stage) {
        this.primaryStage = stage;
    }

    private Playfair item;

    public void btnEncryptPressed() {

        if (textAreaInput.getText().length() % 2 != 0) {
            throw new IllegalArgumentException("INCORRECT INPUT");
        }
        textAreaOutput.setText(item.encryptMessage(textAreaInput.getText()));
    }

    public void btnDecryptPressed() {
        if (textAreaInput.getText().length() % 2 != 0) {
            throw new IllegalArgumentException("INCORRECT INPUT");
        }
        textAreaOutput.setText(item.decryptMessage(textAreaInput.getText()));

    }


    //MENU
    @FXML
    public void openMenuItemPressed() {
        System.out.println("open CLICKED");
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt"));
        File f = fc.showOpenDialog(primaryStage);
        textAreaInput.clear();
        if (f != null) {
            try {

                //  new FileInputStream(f);
                Scanner s = new Scanner(f);
                while (s.hasNext()) {

                    textAreaInput.appendText(s.next() + "\n");
                }

            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else
            System.out.println("file is null");
    }

    @FXML
    public void saveMenuItemPressed() {
        System.out.println("save CLICKED");

        FileChooser fileChooser = new FileChooser();

        //Set extension filter
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt"));

        //Show save file dialog
        File file = fileChooser.showSaveDialog(primaryStage);

        if (file != null) {
            try {
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write(textAreaOutput.getText());
                fileWriter.close();
            } catch (IOException ex) {
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else System.out.println("file is null");
    }

    @FXML
    public void closeMenuItemPressed() {

        Platform.exit();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        item = new Playfair();
        textFieldKey.textProperty().addListener(
                (observable, oldValue, newValue) ->
                        textAreaTableKey.setText(item.getTableKey(newValue))
        );

        textAreaTableKey.setText(item.getTableKey(textFieldKey.getText()));
    }
}
